import threading


class MyThread(threading.Thread):
    def __init__(self, func):
        """creates a new thread"""
        self.running = False
        self.function = func
        super(MyThread, self).__init__()

    def start(self):
        self.running = True
        super(MyThread, self).start()

    def run(self):
        while self.running:
            self.function()

    def stop(self):
        self.running = False

"""Server for multi-threaded chat application."""
from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import datetime
import time
from MyThread import *

ADMINS = ["George"]
HOST = ''
PORT = 35001
BUFSIZ = 1024
ADDR = (HOST, PORT)
ADDRESSES = dict()  # key is client socket, value is client address
CLIENTS = {}
global server


def accept_incoming_connections():
    """Sets up handling for incoming clients."""
    global server
    while True:
        client, client_address = server.accept()  # connecting to client
        print("{0} has connected.".format(client_address[0]))
        ADDRESSES[client] = client_address[0]
        client.send("Hello! Type your name and press enter")
        Thread(target=handle_client, args=(client, )).start()  # starting a thread to communicate with the client


def handle_client(client):  # Takes client socket as argument.
    """Handles a single client connection."""
    name = get_user_name(client)  # getting the name of the client
    welcome = 'Welcome {0}! If you ever want to quit, send "quit" to exit.'.format(name)
    client.send(welcome)
    time.sleep(0.1)

    """message of current online users"""
    connected = get_connected_status()
    if connected:
        client.send(connected)

    broadcast("{0} has joined the chat!".format(name))  # Notify the users that you are connected

    CLIENTS[client] = name  # saving the client socket with his name

    control_user_messages(client)


def is_admin_command(command):
    """checks if a specific command is admin command"""
    words = command.split()
    if words[0] == "/kick" and len(words) == 2 and words[1] in CLIENTS.values() and words[1][0] != '@':
        return True
    elif words[0] == "/admin" and len(words) == 2 and words[1] in CLIENTS.values() and words[1][0] != '@':
        return True
    elif words[0] == "/mute" and len(words) == 2 and words[1] in CLIENTS.values() and words[1][0] != '@':
        return True
    else:
        return False


def broadcast(msg, prefix=""):  # prefix is for name identification.
    """Broadcasts a message to all the clients."""
    for sock in CLIENTS:
        sock.send(get_time()+prefix+msg)


def get_connected_status():
    """returns a string to show who is online or None if no one is """
    if len(CLIENTS.values()) == 0:
        return "You are the only one in the chat right now."
    elif len(CLIENTS.values()) == 1:
        return "{0} is connected. Talk to him!".format(CLIENTS.values()[0])
    else:
        connected = ""
        for i in range(len(CLIENTS.values())-2):
            connected += "{0}, ".format(CLIENTS.values()[i])
        connected += "{0} ".format(CLIENTS.values()[-2])
        connected += "and {0} are connected. Talk to them!".format(CLIENTS.values()[-1])
        return connected


def get_user_name(client):
    """gives a vaild user name from the client"""
    name = client.recv(BUFSIZ)
    while True:
        if name.find(' ') == -1 and not (name in CLIENTS.values()) and name[0] != '@':
            break
        client.send('You can\'t use the name: "{0}". Please try a different name:'.format(name))
        name = client.recv(BUFSIZ)
    if name in ADMINS:
        name = '@'+name
    return name


def control_user_messages(client):
    """checks every client message and works accordingly"""
    while True:
        msg = client.recv(BUFSIZ)
        if msg != "quit":
            if CLIENTS[client][0] == '@' and is_admin_command(msg):
                if msg.split()[0] == "/kick":
                    broadcast("{0} was kicked from the chat.".format(msg.split()[1]))
                    time.sleep(0.1)
                    get_client_by_name(msg.split()[1]).send("You have been kicked from the chat!")
                elif msg.split()[0] == "/admin":
                    broadcast("{0} is an admin now.".format(msg.split()[1]))
                    add_to_admins(msg.split()[1])
                else:  # must to be mute command
                    get_client_by_name(msg.split()[1]).send("You have been muted!")
            elif msg == "view-managers":
                client.send("The managers are: " + ', '.join(ADMINS))
            elif msg.split()[0] == "/pm" and len(msg.split()) >= 3 and (msg.split()[1] in CLIENTS.values()):
                message = msg.split()
                message_to_send = message[2:]
                str_message_to_send = ' '.join(message_to_send)
                final_msg = get_time() + "!"+CLIENTS[client] + ": " + str_message_to_send
                get_client_by_name(message[1]).send(final_msg)
                client.send(final_msg)

            else:
                broadcast(msg, CLIENTS[client] + ": ")
        else:
            disconnect_user(client)
            break


def get_client_by_name(name):
    """gets the name of the client and retunrs his socket"""
    for every_client, client_name in CLIENTS.iteritems():
        if client_name == name:
            return every_client


def disconnect_user(client):
    print("{0} has disconnected.".format(ADDRESSES[client]))
    broadcast("{0} left the chat.".format(CLIENTS[client]))
    client.close()
    del CLIENTS[client]


def get_time():
    """returns time string -> [hour]:[minute] """
    now = datetime.datetime.now()
    return '{d.hour}:{d.minute:02} '.format(d=now)


def add_to_admins(name):
    """adds an admin"""
    new_admin = "@"+name
    for client, client_name in CLIENTS.iteritems():
        if client_name == name:
            CLIENTS[client] = new_admin
            time.sleep(0.1)
            client.send("You are an admin now")
            break
    ADMINS.append(name)


def main():
    global server
    server = socket(AF_INET, SOCK_STREAM)
    server.bind(ADDR)
    server.listen(5)
    print("Waiting for connection...")
    accept_thread = MyThread(accept_incoming_connections)  # Thread(target=accept_incoming_connections)
    accept_thread.start()
    accept_thread.join()
    server.close()


if __name__ == "__main__":
    main()

#!/usr/bin/env python3
"""Script for Tkinter GUI chat client."""
from socket import AF_INET, socket, SOCK_STREAM
import Tkinter as tkinter
import tkMessageBox
from MyThread import *

HOST = "127.0.0.1"
PORT = 35001
BUFSIZ = 1024
ADDR = (HOST, PORT)


def receive():
    """Handles receiving of messages."""
    while True:
        try:
            msg = client_socket.recv(BUFSIZ)  # gets messages from the server

            # if the server closed the socket or you have been kicked
            if not msg or msg == "You have been kicked from the chat!":
                msg_list.insert(tkinter.END, msg)
                entry_field.config(state='disabled')
                send_button.config(state='disabled')
            elif msg == "You have been muted!":
                send_button.config(state='disabled')
                entry_field.option_clear()
                entry_field.insert(0, "You cannot speak here")
                entry_field.config(state='disabled')
            else:
                # displays the message on screen
                msg_list.insert(tkinter.END, msg)
                msg_list.see(tkinter.END)  # automatic scroll down
        except:
            # if the users closes the app
            top.quit()
            receive_thread.stop()
            break


def send(event=None):  # event is passed by binders.
    """Handles sending of messages."""
    msg = my_msg.get()
    my_msg.set("")  # Clears input field.
    client_socket.send(msg)
    if msg == "quit":
        client_socket.close()
        top.quit()


def ask_quit():
    if tkMessageBox.askokcancel("Quit", "You want to quit now?"):
        client_socket.send("quit")
        client_socket.close()
        top.quit()


if __name__ == '__main__':
    top = tkinter.Tk()
    top.title("George's Chat!")
    top.geometry("500x560")  # size of the app
    top.resizable(0, 0)  # Don't allow resizing in the x or y direction

    messages_frame = tkinter.Frame(top)
    my_msg = tkinter.StringVar()  # For the messages to be sent.
    scrollbar = tkinter.Scrollbar(messages_frame)  # To navigate through past messages.
    # Following will contain the messages.
    msg_list = tkinter.Listbox(messages_frame, height=30, width=100, yscrollcommand=scrollbar.set)
    scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
    msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
    msg_list.pack()
    messages_frame.pack()

    entry_field = tkinter.Entry(top, textvariable=my_msg, width=50)
    entry_field.bind("<Return>", send)
    entry_field.pack()
    send_button = tkinter.Button(top, text="Send", command=send, width=20, height=2)
    send_button.pack()

    client_socket = socket(AF_INET, SOCK_STREAM)
    client_socket.connect(ADDR)

    receive_thread = MyThread(receive)
    receive_thread.start()
    top.protocol("WM_DELETE_WINDOW", ask_quit)
    top.mainloop()  # Starts GUI execution.
